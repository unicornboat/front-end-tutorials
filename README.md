# UB的前端收藏

#### 说明
教室里会逐渐根据一些常见的需求，增加不同类型的前端代码。如果在这里找不到答案和例子的话，可以在搜索引擎和各个网站上继续寻找，也可以在[issues](https://gitee.com/unicornboat/front-end-tutorials/issues)里面提出问题。

#### 教程目录
- [DIV自适应居中](https://gitee.com/unicornboat/front-end-tutorials/tree/master/examples/DIV%E8%87%AA%E9%80%82%E5%BA%94%E5%B1%85%E4%B8%AD)
- [H5全屏翻页](https://gitee.com/unicornboat/front-end-tutorials/tree/master/examples/H5%E5%85%A8%E5%B1%8F%E7%BF%BB%E9%A1%B5)
- [纯CSS轮播](https://gitee.com/unicornboat/front-end-tutorials/tree/master/examples/%E7%BA%AFCSS%E8%BD%AE%E6%92%AD)
    - 自动播放
    - 无限循环
    - 鼠标悬浮在轮播上时自动暂停
    - 不兼容IE