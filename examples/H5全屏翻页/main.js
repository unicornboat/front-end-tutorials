function afterMouseWheelEnd (el, cb) {
	var data = {x: 0, y: 0}, faction = 0.01, wheeling;
	el.addEventListener('wheel', function (e) {
		clearTimeout(wheeling);
		wheeling = setTimeout(function() {
			wheeling = undefined;
			if (typeof cb === 'function') cb(data);
			data = {x: 0, y: 0};
		}, 100);
		data.x += e.deltaX * faction;
		data.y += e.deltaY * faction;
	});
}

function afterSwipeEnd (el, cb) {
	var data, pos;
	el.addEventListener('touchstart', function (e) {
		data = {x: 0, y: 0};
		pos = {x: e.touches[0].clientX, y: e.touches[0].clientY};
	});
	el.addEventListener('touchend', function () {
		if (typeof cb === 'function') cb(data);
	});
	el.addEventListener('touchmove', function (e) {
		data = {x: e.touches[0].clientX - pos.x, y: e.touches[0].clientY - pos.y};
		pos = {x: e.touches[0].clientX, y: e.touches[0].clientY};
	});
}

class H5FullPageScroller {
	#container = null;
	#scrolling = false;
	#settings = {
		arrowClass: 'h5f-page-toggler',
		containerClass: 'h5f-container',
		contentClass: 'h5f-content',
		duration: 200,
		indexAttribute: 'data-h5f-index',
		offsetAttribute: 'data-h5f-offset',
		pageClass: 'h5f-page'
	};

	constructor (q) {
		if (typeof q === 'string') {
			if (q.trim() === '') throw '存放页面的选择器字串不能为空或者单纯的空格';
			if (document.querySelector(q) === null) throw '选择器"' + q + '"没有指向一个有效的DOM元素，请检查选择器字串';
			this.#container = document.querySelector(q);
		} else if (typeof q === 'object' && q.constructor.toString.indexOf('HTML') > -1) {
			this.#container = q;
		} else if (document.querySelector('.h5f-container') !== null) {
			this.#container = document.querySelector('.h5f-container');
		}

		if (this.#container) {
			this.#container.classList.add(this.#settings.containerClass);
		} else {
			throw '定义H5全屏翻页容器失败';
		}

		this.#getArrows().forEach(arrow => {
			if (arrow.hasAttribute(this.#settings.offsetAttribute)) {
				arrow.addEventListener('click', e => {
					this.togglePage(parseInt(arrow.getAttribute(this.#settings.offsetAttribute)));
				});
			}
		});

		afterMouseWheelEnd(document, e => {
			if (this.#scrolling || e.y === 0) return;
			this.togglePage(e.y > 0 ? 1 : -1);
		});

		afterSwipeEnd(document, e => {
			if (this.#scrolling || e.y === 0) return;
			this.togglePage(e.y > 0 ? -1 : 1);
		});
	}

	#getArrows () {
		return this.#container.querySelectorAll('.' + this.#settings.arrowClass);
	}

	#getPageCount () {
		return this.#getPages().length;
	}

	#getPages () {
		let pages = this.#container.querySelectorAll('.' + this.#settings.pageClass);
		if (!pages.length) throw '在定义的容器下没找到任何预设的页面元素';
		return pages;
	}

	togglePage (offset) {
		let count = 0;
		if (!Number.isInteger(offset)) throw '页面跳转的参数必须是一个整数';
		if (!this.#scrolling) {
			let id,
				max = this.#getPageCount(),
				current = !isNaN(parseInt(this.#container.getAttribute(this.#settings.indexAttribute))) ?
					parseInt(this.#container.getAttribute(this.#settings.indexAttribute)) : 1,
				target = current + offset;
			if (target < 0) target = 1;
			if (target > max) target = max;
			this.#container.setAttribute(this.#settings.indexAttribute, target);

			id = setTimeout(() => {
				this.#scrolling = false;
				clearTimeout(id);
			}, this.#settings.duration + 10);
		}
	}
}
(function () {
	new H5FullPageScroller();
})();