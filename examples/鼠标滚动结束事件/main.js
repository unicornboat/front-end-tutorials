function afterMouseWheelEnd (el, cb) {
	var data = {x: 0, y: 0}, faction = 0.01, wheeling;
	el.addEventListener('wheel', function (e) {
		clearTimeout(wheeling);
		wheeling = setTimeout(function() {
			wheeling = undefined;
			if (typeof cb === 'function') cb(data);
			data = {x: 0, y: 0};
		}, 100);
		data.x += e.deltaX * faction;
		data.y += e.deltaY * faction;
	});
}

(function () {
	afterMouseWheelEnd(document.querySelector('.cube'), function (data) {

	});
})();